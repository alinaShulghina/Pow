package com.softserve.edu.pow;

import org.junit.Test;

import static com.softserve.edu.pow.MathOperations.pow;
import static org.junit.Assert.*;

/**
 * Created by alin- on 14.12.2017.
 */
public class MathOperationsTest {
    private static final double DELTA = 0.01;

    @Test
    public void powTestNegativePow(){
        assertEquals(0.04,pow(5,-2),DELTA);
    }

    @Test
    public void powTestZeroPow(){
        assertEquals(1,pow(5,0),DELTA);
    }

    @Test
    public void powTestOnePow(){
        assertEquals(100,pow(100,1),DELTA);
    }

    @Test
    public void powTestPositivePow(){
        assertEquals(4294967296.0,pow(2,32),DELTA);
    }
}