package com.softserve.edu.pow;

/**
 * Created by alin- on 14.12.2017.
 */
public class MathOperations {

    public static double pow(double x, int n) {
        if (n == 0) {
            return 1;
        }
        if (n < 0) {
            return pow(1 / x, -n);
        }
        if (n % 2 == 1) {
            return (pow(x, n - 1) * x);
        } else {
            double res = pow(x, n / 2);
            return res * res;
        }
    }
}
