package com.softserve.edu.pow;

/**
 * Created by alin- on 12.12.2017.
 */
public class Main {

    public static void main(String[] args) {
        try {
            int x = Integer.valueOf(args[0]);
            int n = Integer.valueOf(args[1]);
            System.out.print(x + "^" + n + " = " + MathOperations.pow(x, n));
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("You should input 2 params!");
        } catch (NumberFormatException e) {
            System.out.println("Strings are not allowed!");
        }
    }


}
